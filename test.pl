#!/usr/bin/perl

##################################
#tgorn(at)wp.pl 2018Apr07
##################################
#ver 0.1 2018Apr07  
#ver 0.2 2018Apr12 @22:45 
#
use strict;
use warnings;

use REST::Client;
use JSON;
use MIME::Base64;
use LWP::Simple;
use Data::Dumper qw(Dumper);
use 5.010;

sub uniq {
  my %seen;
  #return grep { ! $seen{$_}++ } @_;
  return grep { ! $seen{ join $;, @$_ }++,} @_;
}

=b
my %h_main;

$h_main{"tutorials"}{"Tomek Gorny"}{"total_commits_share_percentage"} = 300;
$h_main{"tutorials"}{"Tomek Gorny"}{"git_repos"}{"repo1"} = 100;
$h_main{"tutorials"}{"Tomek Gorny"}{"git_repos"}{"repo2"} = 200;

print "---Start--------\n";
print "----------------\n";
print "----------------\n";
print "----------------\n";
print "----------------\n";
print Dumper \%h_main;
print "----------------\n";
print "----------------\n";
print "----------------\n";
print "----------------\n";
print "---End----------\n";

my $json = encode_json \%h_main;


print "\n$json\n";

=cut

print "\n Bitbucket query : Start";

my $username = 'Tomektg';
my $password = '';

my $auth = MIME::Base64::encode_base64($username . ':' . $password);
my $client = REST::Client->new();
my $headers = {Accept => 'Content-Type: application/json', Authorization => 'Basic' . $auth };

$client->setHost('https://api.bitbucket.org');
$client->GET( '/2.0/repositories/Tomektg/spartez_test_00/commits/master');

my $resp = from_json( $client->responseContent() );
print "\n Bitbucket query : End\n";

=b
print "----Dumper-------\n";
print "----------------\n";
print "----------------\n";
print Dumper($resp);
print "----------------\n";
print "----------------\n";
print "----Dumper-End -\n";
=cut	

my @a_items = @{$resp->{'values'} };

my $author_raw = "";
my $repo_name = "";
my $is_commit = "";		

my $rec = {};
my @AoH = ();
my @AoH_Final = ();


foreach my $i_item ( @a_items ) {
	my $rec_Author = {};
	my $rec_repo_hash = {};

	my $repo_name = $i_item->{'repository'}->{'name'};
	my $comm_hash = $i_item->{'hash'};
	my $is_commit = $i_item->{'type'};
	my $author_raw = $i_item->{'author'}->{'raw'};


	if ( $is_commit eq "commit") {
		$rec_repo_hash -> {$repo_name} = $comm_hash;
		$rec_Author->{ $author_raw }   = $rec_repo_hash;
		push (@AoH, $rec_Author);
	}	
	
}

=b
print Dumper \@AoH , "\n";
=cut

my @AoU = ();

for  my $i ( 00 .. $#AoH ) {
	foreach my $j ( keys %{$AoH[$i]} ){
		foreach my $k ( keys %{ $AoH[$i]{$j} } ) {
			push (@AoU ,  [ $j , $k] ); 
		}
	}
}

#Array of 
my @unique_users = uniq (@AoU);

my $rec_repo = ();
my $rec_user = ();

for my $c ( 00 .. $#unique_users){
	#print "\nrepo for ::",  $unique_users[$c][0], " repo_name::", $unique_users[$c][1] ,":\n";
	my @a_tmp_1 = ();
	my $rec = ();
	
	for my $i ( 00 .. $#AoH ) {
		foreach my $j ( keys %{ $AoH[$i] } ) { 
			foreach my $k ( keys %{ $AoH[$i]{$j} }){
					my $l =  $AoH[$i]{$j}{$k};
					if  ( ($j eq $unique_users[$c][0] ) and ($k eq $unique_users[$c][1] ) ) { 
						push (@a_tmp_1, $l);
					}
			}
		}
	

	}
	#print "\n", $unique_users[$c][0] , ":x:",  Dumper \@a_tmp_1, "\n";
	$rec_repo -> {$unique_users[$c][1]} = [@a_tmp_1 ];
	$rec_user -> {$unique_users[$c][0]} = $rec_repo ;
	push (@AoH_Final, $rec_user); 
	$rec_repo = ();
	$rec_user = ();
}

=b
print "\nRepo Dump Start\n";
say Dumper \@unique_users;
print "\nRepo Dump End\n";
=cut

print "\n Dump AoH_Final\n";
print Dumper \@AoH_Final; 

=b
print "\nJSon_1\n";
my $json_1 = encode_json $AoH[0];
print "\n$json_1\n";
=cut

=b
https://developer.atlassian.com/server/fisheye-crucible/writing-a-rest-client-in-perl/

Anonymouse array of hases : from_json
http://www.perlmonks.org/?node_id=651544
=cut
